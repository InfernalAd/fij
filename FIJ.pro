#-------------------------------------------------
#
# Project created by QtCreator 2012-10-23T17:06:21
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = FIJ
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    lexer/lexeme.cpp \
    lexer/lexemebuffer.cpp \
    lexer/lexer.cpp \
    codegenerator/codegenerator.cpp \
    parsetree/nodes/ptstatements.cpp \
    parsetree/nodes/ptprogram.cpp \
    parsetree/nodes/ptlogicrelation.cpp \
    parsetree/nodes/ptblock.cpp \
    parsetree/nodes/ptbinaryexpression.cpp \
    parsetree/nodes/expressions/ptsubexpression.cpp \
    parsetree/nodes/expressions/ptparenexpression.cpp \
    parsetree/nodes/expressions/ptnumberexpression.cpp \
    parsetree/nodes/expressions/ptmulexpression.cpp \
    parsetree/nodes/expressions/ptidentifierexpression.cpp \
    parsetree/nodes/expressions/ptfloatnumberexpression.cpp \
    parsetree/nodes/expressions/ptdivexpression.cpp \
    parsetree/nodes/expressions/ptaddexpression.cpp \
    parsetree/nodes/logicexpressions/ptorexpression.cpp \
    parsetree/nodes/logicexpressions/ptnotexpression.cpp \
    parsetree/nodes/logicexpressions/ptnotequalexpression.cpp \
    parsetree/nodes/logicexpressions/ptmoreexpression.cpp \
    parsetree/nodes/logicexpressions/ptmoreequalexpression.cpp \
    parsetree/nodes/logicexpressions/ptlogicparenexpression.cpp \
    parsetree/nodes/logicexpressions/ptlessexpression.cpp \
    parsetree/nodes/logicexpressions/ptlessequalexpression.cpp \
    parsetree/nodes/logicexpressions/ptequalexpression.cpp \
    parsetree/nodes/logicexpressions/ptandexpression.cpp \
    parsetree/nodes/statements/ptwritestatement.cpp \
    parsetree/nodes/statements/ptwhilestatement.cpp \
    parsetree/nodes/statements/ptsqrtstatement.cpp \
    parsetree/nodes/statements/ptreadstatement.cpp \
    parsetree/nodes/statements/ptmodstatement.cpp \
    parsetree/nodes/statements/ptifstatement.cpp \
    parsetree/nodes/statements/ptdivstatement.cpp \
    parsetree/nodes/statements/ptdeclarationstatement.cpp \
    parsetree/nodes/statements/ptassignstatement.cpp \
    codegenerator/asmwriter.cpp \
    csemantic.cpp \
    codegenerator/ptcharstable.cpp \
    parser/parser.cpp \
    parser/parserstate.cpp \
    parsetree/nodes/expressions/ptstringexpression.cpp

HEADERS += \
    lexer/lexeme.h \
    lexer/lexemebuffer.h \
    lexer/lexer.h \
    codegenerator/codegenerator.h \
    parsetree/ptvisitor.h \
    parsetree/ptnode.h \
    parsetree/nodes/ptwriteexpression.h \
    parsetree/nodes/ptstatements.h \
    parsetree/nodes/ptstatement.h \
    parsetree/nodes/ptprogram.h \
    parsetree/nodes/ptlogicrelation.h \
    parsetree/nodes/ptlogicexpression.h \
    parsetree/nodes/ptexpression.h \
    parsetree/nodes/ptblock.h \
    parsetree/nodes/ptbinaryexpression.h \
    parsetree/nodes/nodes.h \
    parsetree/nodes/expressions/ptsubexpression.h \
    parsetree/nodes/expressions/ptparenexpression.h \
    parsetree/nodes/expressions/ptnumberexpression.h \
    parsetree/nodes/expressions/ptmulexpression.h \
    parsetree/nodes/expressions/ptidentifierexpression.h \
    parsetree/nodes/expressions/ptfloatnumberexpression.h \
    parsetree/nodes/expressions/ptdivexpression.h \
    parsetree/nodes/expressions/ptaddexpression.h \
    parsetree/nodes/logicexpressions/ptorexpression.h \
    parsetree/nodes/logicexpressions/ptnotexpression.h \
    parsetree/nodes/logicexpressions/ptnotequalexpression.h \
    parsetree/nodes/logicexpressions/ptmoreexpression.h \
    parsetree/nodes/logicexpressions/ptmoreequalexpression.h \
    parsetree/nodes/logicexpressions/ptlogicparenexpression.h \
    parsetree/nodes/logicexpressions/ptlessexpression.h \
    parsetree/nodes/logicexpressions/ptlessequalexpression.h \
    parsetree/nodes/logicexpressions/ptequalexpression.h \
    parsetree/nodes/logicexpressions/ptandexpression.h \
    parsetree/nodes/statements/ptwritestatement.h \
    parsetree/nodes/statements/ptwhilestatement.h \
    parsetree/nodes/statements/ptsqrtstatement.h \
    parsetree/nodes/statements/ptreadstatement.h \
    parsetree/nodes/statements/ptmodstatement.h \
    parsetree/nodes/statements/ptifstatement.h \
    parsetree/nodes/statements/ptdivstatement.h \
    parsetree/nodes/statements/ptdeclarationstatement.h \
    parsetree/nodes/statements/ptassignstatement.h \
    codegenerator/asmwriter.h \
    csemantic.h \
    codegenerator/ptcharstable.h \
    parser/parser.h \
    parser/parserstate.h \
    parsetree/nodes/expressions/ptstringexpression.h
