#include "asmwriter.h"

ASMWriter::ASMWriter(std::ostream *out)
    :_out(out)
{

}

void ASMWriter::writeHeader()
{
    this->writeLn(".386");
    this->writeLn(".model flat,stdcall");
    this->writeLn("option casemap:none");

    this->writeLn("includelib kernel32.lib");
    this->writeLn("include windows.inc");
    this->writeLn("include kernel32.inc");
    this->writeLn("include masm32rt.inc");
    this->writeLn(".data");
    this->writeLn("outBuffer	db	''");
}

void ASMWriter::write(std::string str)
{
    *_out << str;
}

void ASMWriter::writeLn(std::string str)
{
    *_out << str << std::endl;
}

void ASMWriter::writeLn()
{
    *_out << std::endl;
}
