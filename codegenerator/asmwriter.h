#ifndef ASMWRITER_H
#define ASMWRITER_H
#include "iostream"
#include "string"

class ASMWriter
{
public:
    ASMWriter(std::ostream *);

    void writeHeader();




    void write(std::string);
    void writeLn(std::string);
    void writeLn();
private:
    std::ostream * _out;
};

#endif // ASMWRITER_H
