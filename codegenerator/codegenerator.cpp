#include "codegenerator.h"

CodeGenerator::CodeGenerator()
{
    _writer = new ASMWriter(&std::cout);
    _whileDepth = 0;
    _ifDepth = 0;
}

void CodeGenerator::setCharTable(PTCharsTable *table)
{
    _charTable = table;
}

void CodeGenerator::visit(PTProgram * program)
{
    _writer->writeHeader();
    _writer->writeLn(".code");
    _writer->writeLn("Main PROC");

    program->block()->accept(this);
    _writer->writeLn("invoke Sleep,2500d");
    _writer->writeLn("invoke ExitProcess,NULL");
    _writer->writeLn("Main ENDP");
    _writer->writeLn("end Main");
}



void CodeGenerator::visit(PTBlock * block)
{
    block->statements()->accept(this);
}

void CodeGenerator::visit(PTStatements * statements)
{
    for (int i=0;i!=statements->statementsNum();i++){
        statements->statement(i)->accept(this);
    }
}

void CodeGenerator::visit(PTDeclarationStatement *)
{
    //Do nothing
}

void CodeGenerator::visit(PTWhileStatement * whilestmt)
{
    _whileDepth++;
    _writer->writeLn("while"+QString::number(_whileDepth).toStdString()+":");
    this->_thenStmts = whilestmt->statements();
    whilestmt->expression()->accept(this);
    for (int i=0;i!=_ifDepth;i++){
        _writer->writeLn(".endif");
    }
    _ifDepth = 0;
    _whileDepth--;
}

void CodeGenerator::visit(PTIfStatement * ifstmt)
{
    this->_thenStmts = ifstmt->thenStatements();
    this->_elseStmts = ifstmt->elseStatements();
    ifstmt->expression()->accept(this);
    for (int i=0;i!=_ifDepth;i++){
        _writer->writeLn(".endif");
    }
    _ifDepth = 0;

}

void CodeGenerator::visit(PTAssignStatement * stmt)
{
    /*if (_charTable != NULL){
        if (_charTable->exists(stmt->name())){
            VAR::VariableType type = _charTable->type(stmt->name());
            switch (type){
                case VAR::INT:
                    _writer->write("mov " + stmt->name().toStdString() + "," + stmt->value().toStdString());
                break;
                case VAR::FLOAT:
                break;
                case VAR::STRING:
                     _writer->write("mov " + stmt->name().toStdString() + "," + stmt->value().toStdString());
                break;
                default:
                //Do nothing
                break;
            }
        }
    }*/
}

void CodeGenerator::visit(PTReadStatement *)
{
    qDebug() << "PTReadStatement";
}

void CodeGenerator::visit(PTWriteStatement * stmt)
{
    VAR::VariableType type = VAR::STRING;
    PTExpression * expr = stmt->expression();
    switch(type){
        case VAR::INT:
            _writer->writeLn("printf(\"%i \\n\"," + expr->toString().toStdString()+")");
        break;
        case VAR::FLOAT:
            _writer->writeLn("printf(\"%.2f \\n\"," + expr->toString().toStdString()+")");
        break;
        case VAR::STRING:
            _writer->writeLn("printf(\"" + expr->toString().toStdString()+"\")");
        break;
        default:
            qDebug() << "errorneus type in PTWriteExpression";
        break;
    }

}

void CodeGenerator::visit(PTModStatement *)
{
    qDebug() << "PTModStatement";
}

void CodeGenerator::visit(PTDivStatement *)
{
    qDebug() << "PTDivStatement";
}

void CodeGenerator::visit(PTSqrtStatement *)
{
    qDebug() << "PTSqrtStatement";
}

void CodeGenerator::visit(PTAddExpression *)
{
}

void CodeGenerator::visit(PTSubExpression *)
{
}

void CodeGenerator::visit(PTMulExpression *)
{
}

void CodeGenerator::visit(PTDivExpression *)
{
}

void CodeGenerator::visit(PTNumberExpression *)
{
}

void CodeGenerator::visit(PTFloatNumberExpression *)
{
}

void CodeGenerator::visit(PTStringExpression *)
{
}

void CodeGenerator::visit(PTIdentifierExpression *)
{
}

void CodeGenerator::visit(PTParenExpression *)
{
}

void CodeGenerator::visit(PTAndExpression *expr)
{
    _ifDepth++;
    expr->leftExpr()->accept(this);
    expr->rightExpr()->accept(this);
    this->_thenStmts->accept(this);
    if (_whileDepth>0){
        _writer->writeLn("jmp while"+QString::number(_whileDepth).toStdString());
    }
    _ifDepth--;
}

void CodeGenerator::visit(PTEqualExpression *expr)
{
    if (expr->leftExpr()->isSimple() && expr->rightExpr()->isSimple()){
        _writer->writeLn("mov eax,"+expr->leftExpr()->toString().toStdString());
        _writer->writeLn("mov ebx,"+expr->rightExpr()->toString().toStdString());
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    else if (!expr->leftExpr()->isSimple() && expr->rightExpr()->isSimple()){
        this->parseExpression(expr->leftExpr());//Result in eax
        _writer->writeLn("mov ebx,"+expr->rightExpr()->toString().toStdString());
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    else if (!expr->leftExpr()->isSimple() && !expr->rightExpr()->isSimple()){
        this->parseExpression(expr->leftExpr());//Result in eax
        _writer->writeLn("push eax");
        this->parseExpression(expr->rightExpr());//Result in eax
        _writer->writeLn("mov ebx,eax");
        _writer->writeLn("pop eax");
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
}

void CodeGenerator::visit(PTLessEqualExpression *expr)
{
    if (expr->leftExpr()->isSimple() && expr->rightExpr()->isSimple()){
        _writer->writeLn("mov eax,"+expr->leftExpr()->toString().toStdString());
        _writer->writeLn("mov ebx,"+expr->rightExpr()->toString().toStdString());
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    else if (!expr->leftExpr()->isSimple() && expr->rightExpr()->isSimple()){
        this->parseExpression(expr->leftExpr());//Result in eax
        _writer->writeLn("mov ebx,"+expr->rightExpr()->toString().toStdString());
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    else if (!expr->leftExpr()->isSimple() && !expr->rightExpr()->isSimple()){
        this->parseExpression(expr->leftExpr());//Result in eax
        _writer->writeLn("push eax");
        this->parseExpression(expr->rightExpr());//Result in eax
        _writer->writeLn("mov ebx,eax");
        _writer->writeLn("pop eax");
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
}

void CodeGenerator::visit(PTLessExpression *expr)
{
    if (expr->leftExpr()->isSimple() && expr->rightExpr()->isSimple()){
        _writer->writeLn("mov eax,"+expr->leftExpr()->toString().toStdString());
        _writer->writeLn("mov ebx,"+expr->rightExpr()->toString().toStdString());
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    else if (!expr->leftExpr()->isSimple() && expr->rightExpr()->isSimple()){
        this->parseExpression(expr->leftExpr());//Result in eax
        _writer->writeLn("mov ebx,"+expr->rightExpr()->toString().toStdString());
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    else if (!expr->leftExpr()->isSimple() && !expr->rightExpr()->isSimple()){
        this->parseExpression(expr->leftExpr());//Result in eax
        _writer->writeLn("push eax");
        this->parseExpression(expr->rightExpr());//Result in eax
        _writer->writeLn("mov ebx,eax");
        _writer->writeLn("pop eax");
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    if (_ifDepth == 1){
        _thenStmts->accept(this);
    }
}

void CodeGenerator::visit(PTLogicParenExpression *)
{
}

void CodeGenerator::visit(PTMoreEqualExpression *expr)
{
    if (expr->leftExpr()->isSimple() && expr->rightExpr()->isSimple()){
        _writer->writeLn("mov eax,"+expr->leftExpr()->toString().toStdString());
        _writer->writeLn("mov ebx,"+expr->rightExpr()->toString().toStdString());
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    else if (!expr->leftExpr()->isSimple() && expr->rightExpr()->isSimple()){
        this->parseExpression(expr->leftExpr());//Result in eax
        _writer->writeLn("mov ebx,"+expr->rightExpr()->toString().toStdString());
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    else if (!expr->leftExpr()->isSimple() && !expr->rightExpr()->isSimple()){
        this->parseExpression(expr->leftExpr());//Result in eax
        _writer->writeLn("push eax");
        this->parseExpression(expr->rightExpr());//Result in eax
        _writer->writeLn("mov ebx,eax");
        _writer->writeLn("pop eax");
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
}

void CodeGenerator::visit(PTMoreExpression *expr)
{
    if (expr->leftExpr()->isSimple() && expr->rightExpr()->isSimple()){
        _writer->writeLn("mov eax,"+expr->leftExpr()->toString().toStdString());
        _writer->writeLn("mov ebx,"+expr->rightExpr()->toString().toStdString());
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    else if (!expr->leftExpr()->isSimple() && expr->rightExpr()->isSimple()){
        this->parseExpression(expr->leftExpr());//Result in eax
        _writer->writeLn("mov ebx,"+expr->rightExpr()->toString().toStdString());
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    else if (!expr->leftExpr()->isSimple() && !expr->rightExpr()->isSimple()){
        this->parseExpression(expr->leftExpr());//Result in eax
        _writer->writeLn("push eax");
        this->parseExpression(expr->rightExpr());//Result in eax
        _writer->writeLn("mov ebx,eax");
        _writer->writeLn("pop eax");
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
}

void CodeGenerator::visit(PTNotEqualExpression *expr)
{
    if (expr->leftExpr()->isSimple() && expr->rightExpr()->isSimple()){
        _writer->writeLn("mov eax,"+expr->leftExpr()->toString().toStdString());
        _writer->writeLn("mov ebx,"+expr->rightExpr()->toString().toStdString());
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    else if (!expr->leftExpr()->isSimple() && expr->rightExpr()->isSimple()){
        this->parseExpression(expr->leftExpr());//Result in eax
        _writer->writeLn("mov ebx,"+expr->rightExpr()->toString().toStdString());
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
    else if (!expr->leftExpr()->isSimple() && !expr->rightExpr()->isSimple()){
        this->parseExpression(expr->leftExpr());//Result in eax
        _writer->writeLn("push eax");
        this->parseExpression(expr->rightExpr());//Result in eax
        _writer->writeLn("mov ebx,eax");
        _writer->writeLn("pop eax");
        _writer->write(".if");
        _writer->writeLn(" " + expr->toString().toStdString());
        _ifDepth++;
    }
}

void CodeGenerator::visit(PTNotExpression *)
{
}

void CodeGenerator::visit(PTOrExpression * expr)
{
    _ifDepth++;
    expr->leftExpr()->accept(this);
    this->_thenStmts->accept(this);
    if (_whileDepth>0){
        _writer->writeLn("jmp while"+QString::number(_whileDepth).toStdString());
    }
    _writer->writeLn(".else");
    expr->rightExpr()->accept(this);
    this->_thenStmts->accept(this);
    if (_whileDepth>0){
        _writer->writeLn("jmp while"+QString::number(_whileDepth).toStdString());
    }
    if (_ifDepth == 1){
        _thenStmts->accept(this);
    }
    _ifDepth--;
}

void CodeGenerator::parseExpression(PTExpression * expr)
{
    _writer->writeLn("pushad");
    QVector<QString> polish = expr->toPolishNotation();
    for (int i=0;i!=polish.size();i++){
        if (polish[i]=="+"){
            _writer->writeLn("pop eax");
            _writer->writeLn("pop ebx");
            _writer->writeLn("add eax,ebx");
            _writer->writeLn("push eax");
        }
        else if (polish[i]=="-"){
            _writer->writeLn("pop eax");
            _writer->writeLn("pop ebx");
            _writer->writeLn("sub eax,ebx");
            _writer->writeLn("push eax");
        }
        else if (polish[i]=="*"){
            _writer->writeLn("pop eax");
            _writer->writeLn("pop ebx");
            _writer->writeLn("mul ebx");
            _writer->writeLn("push eax");
        }
        else if (polish[i]=="/"){
            _writer->writeLn("pop eax");
            _writer->writeLn("pop ebx");
            _writer->writeLn("div ebx");
            _writer->writeLn("push eax");
        }
        else{
            _writer->writeLn("push " + polish[i].toStdString());
        }
    }
    _writer->writeLn("pop eax");
    _writer->writeLn("popad");
}

void CodeGenerator::declareVariables()
{

}
