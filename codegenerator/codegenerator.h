#ifndef CODEGENERATOR_H
#define CODEGENERATOR_H
#include "parsetree/ptvisitor.h"
#include "parsetree/nodes/nodes.h"
#include "asmwriter.h"
#include "ptcharstable.h"
#include "QDebug"
#include "iostream"
#include "fstream"


class CodeGenerator:public PTVisitor
{
public:
    CodeGenerator();
    void setCharTable(PTCharsTable*);
    void visit(PTProgram *);
    void visit(PTBlock *);
    void visit(PTStatements *);
    void visit(PTDeclarationStatement *);
    void visit(PTWhileStatement *);
    void visit(PTIfStatement *);
    void visit(PTAssignStatement *);
    void visit(PTReadStatement *);
    void visit(PTWriteStatement *);
    void visit(PTModStatement *);
    void visit(PTDivStatement *);
    void visit(PTSqrtStatement *);

    void visit(PTAddExpression *);
    void visit(PTSubExpression *);
    void visit(PTMulExpression *);
    void visit(PTDivExpression *);

    void visit(PTNumberExpression *);
    void visit(PTFloatNumberExpression *);
    void visit(PTStringExpression *);
    void visit(PTIdentifierExpression *);
    void visit(PTParenExpression *);

    void visit(PTAndExpression *);
    void visit(PTEqualExpression *);
    void visit(PTLessEqualExpression *);
    void visit(PTLessExpression *);
    void visit(PTLogicParenExpression *);
    void visit(PTMoreEqualExpression *);
    void visit(PTMoreExpression *);
    void visit(PTNotEqualExpression *);
    void visit(PTNotExpression *);
    void visit(PTOrExpression *);

    void parseExpression(PTExpression *);
    void declareVariables();
private:
    ASMWriter * _writer;
    PTStatements * _thenStmts;
    PTStatements * _elseStmts;
    PTCharsTable * _charTable;
    bool _isWhile;
    int _ifDepth;
    int _whileDepth;
};

#endif // CODEGENERATOR_H
