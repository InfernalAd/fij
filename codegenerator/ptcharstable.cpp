#include "ptcharstable.h"

PTCharsTable::PTCharsTable()
{
}

void PTCharsTable::addVariable(QString name, VAR::VariableType type)
{
    if (!this->exists(name))
    {
        //qDebug() << "Variable" << name << "sucsessfuly added";
        _variables.insert(name,type);
    }
    else
    {
        qDebug() << "Variable" << name << "already exists";
    }
}

bool PTCharsTable::exists(QString name)
{
    return _variables.contains(name);
}

VAR::VariableType PTCharsTable::type(QString name)
{
    if (this->exists(name))
    {
        return _variables.value(name);
    }
    else
    {
        return VAR::UNKNOWN;
    }
}

void PTCharsTable::showMap()
{
    QMap<QString,VAR::VariableType>::iterator it = _variables.begin();
    for(;it != _variables.end(); ++it)
    {
        qDebug() << it.key() << ": " << it.value();
    }
}
