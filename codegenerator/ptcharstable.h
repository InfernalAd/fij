#ifndef PTCHARSTABLE_H
#define PTCHARSTABLE_H
#include "QMap"
#include "../parsetree/ptnode.h"
#include "QString"
#include "QDebug"

class PTCharsTable
{
public:
    PTCharsTable();
    void addVariable(QString name,VAR::VariableType type);
    bool exists(QString name);
    VAR::VariableType type(QString name);
    void showMap();
private:
    QMap<QString,VAR::VariableType> _variables;
};

#endif // PTCHARSTABLE_H
