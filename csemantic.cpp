#include "csemantic.h"



CSemantic::CSemantic()
{
}

void CSemantic::visit(PTProgram *)
{
}

void CSemantic::visit(PTBlock *)
{
}

void CSemantic::visit(PTStatements * statements)
{
    for (int i = 0; i != statements->statementsNum(); i++)
    {
        statements->statement(i)->accept(this);
    }
}

void CSemantic::visit(PTDeclarationStatement * declarations)
{
    QString s1 = declarations->name();
    VAR::VariableType s2 = declarations->type();
    if(declareCheck(s1, s2))
    {
        _charTable.addVariable(s1, s2);
    }
}

void CSemantic::releaseTheTable()
{
   /* for( std::map<QString, int>::iterator ii=_table.begin(); ii!=_table.end(); ++ii)
    {
        qDebug() << (*ii).first << ": " << (*ii).second;
    }*/
    _charTable.showMap();
}

bool CSemantic::declareCheck(QString s1, VAR::VariableType s2) /*�������� int s2 �� VAR::VariableType*/
{
    if(s1 == "")
    {
        std::cout << "error with name" << std::endl;
        return false;
    }
    if(s2 == VAR::UNKNOWN)
    {
        std::cout << "error with type" << std::endl;
        return false;
    }
    /*if(_charTable[s1] != NULL)
    {
        std::cout << "error with another declaration" << std::endl;
        return false;
    }*/
    return true;
}

void CSemantic::visit(PTWhileStatement *)
{
}

void CSemantic::visit(PTIfStatement *)
{
}

void CSemantic::visit(PTAssignStatement * assign)
{
    /*int type = assign->expression()->getType();
    QString name = assign->name();
    std::cout << type << std::endl;
    qDebug() << name;*/
    //assign->
    //bool * ok = 0;
    //bool ok;
    /*������� �������� �����*/
    //QString par1 = assign->name();
    //qDebug() << par1;
    //QString par2 = assign->value();
    //int t = typeCheck(par1, par2);
    //���� ����� ������, ��� ������ ��������� ���������� �� ���������� � ����� ������
    //���� ��� ������ ������
    //���� �� ����� � ��� � ������ ��������������� �������� � ������ ��� � ���������
    /*if(t != 0)
    {
        //std::cout << t << std::endl;
        if(t == 1)
        {
            std::cout << "int found" << std::endl;
            ok = toInt(par2);
        }
        else if(t == 2)
        {
            std::cout << "float found" << std::endl;
            ok = toFloat(par2);
        }
        else if(t == 3)
        {
            std::cout << "string found" << std::endl;
            ok = true;
        }*/

    //���� ����� ������, ��� ������ ��������� ���������� �� ���������� � ����� ������
    //���� ��� ������ ������
    //���� �� ����� � ��� � ������ ��������������� �������� � ������ ���
    /*int par3 = par2.toInt(ok);
    std::cout << ok << std::endl;
    std::cout << par3 << std::endl;*/
    //std::string par3 = par2.toStdString();//bool * ok = 0, int base = 10)const;
    //par2.toInt;
    /*    if(ok == true)
        {
            qDebug() << par1 << "=" << par2;
        }
    }*/
    //std::cout << par3 << std::endl;
}

int CSemantic::typeCheck(QString par1, QString par2)
{
//    bool isFound = false;
    /*����� ���������� � �������*/
/*    for( std::map<QString, int>::iterator ii=_table.begin(); ii!=_table.end(); ++ii)
    {
        //qDebug() << (*ii).first << ": " << (*ii).second;
        if((*ii).first == par1)
        {
            isFound = true;
        }
    }
    if(!isFound)
    {
        std::cout << "There is no variable with this name" << std::endl;
        return 0;
    }
    else
    {
        int type = _table[par1];
        //std::cout << type << std::endl;
        /*���� �����������*/
 /*       if(type == 1)
        {
            return 1; //int
        }
        else if(type == 2)
        {
            return 2; //float
        }
        else if(type == 3)
        {
            return 3; //string
        }
    }*/
    return 0;
}

bool CSemantic::toInt(QString par2)
{
    bool ok;
    int result = par2.toInt(&ok, 10);
    if(ok == false)
    {
        std::cout << "this is not INT" << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

bool CSemantic::toFloat(QString par2)
{
    bool ok;
    float result = par2.toFloat(&ok);
    if(ok == false)
    {
        std::cout << "this is not FLOAT" << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

void CSemantic::visit(PTReadStatement *)
{
}

void CSemantic::visit(PTWriteStatement *)
{
}

void CSemantic::visit(PTModStatement *)
{
}

void CSemantic::visit(PTDivStatement *)
{
}

void CSemantic::visit(PTSqrtStatement *)
{
}

void CSemantic::visit(PTAddExpression * addExpr)
{
    if(addExpr->getType() == VAR::UNKNOWN)
    {
        std::cout << "Error in addExpr" << std::endl;
    }
}

void CSemantic::visit(PTSubExpression * subExpr)
{
    if(subExpr->getType() == VAR::UNKNOWN)
    {
        std::cout << "Error in subExpr" << std::endl;
    }
}

void CSemantic::visit(PTMulExpression * mulExpr)
{
    if(mulExpr->getType() == VAR::UNKNOWN)
    {
        std::cout << "Error in mulExpr" << std::endl;
    }
}

void CSemantic::visit(PTDivExpression * divExpr)
{
    if(divExpr->getType() == VAR::UNKNOWN)
    {
        std::cout << "Error in divExpr" << std::endl;
    }
}

void CSemantic::visit(PTNumberExpression *)
{
}

void CSemantic::visit(PTFloatNumberExpression *)
{
}

void CSemantic::visit(PTIdentifierExpression *)
{
}

void CSemantic::visit(PTParenExpression *)
{
}

void CSemantic::visit(PTOrExpression *)
{
}

void CSemantic::visit(PTNotExpression *)
{
}

void CSemantic::visit(PTNotEqualExpression *)
{
}

void CSemantic::visit(PTMoreExpression *)
{
}

void CSemantic::visit(PTMoreEqualExpression *)
{
}

void CSemantic::visit(PTLogicParenExpression *)
{
}

void CSemantic::visit(PTLessExpression *)
{
}

void CSemantic::visit(PTLessEqualExpression *)
{
}

void CSemantic::visit(PTEqualExpression *)
{
}

void CSemantic::visit(PTAndExpression *)
{
}

void CSemantic::visit(PTStringExpression *)
{
}

