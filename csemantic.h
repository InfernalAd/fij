#ifndef CSEMANTIC_H
#define CSEMANTIC_H

#include "parsetree/ptvisitor.h"
#include "parsetree/nodes/nodes.h"
#include "codegenerator/ptcharstable.h"
#include "QString"
#include "QDebug"
#include "iostream"
#include "string"
#include "sstream"
#include "utility"
#include "map"


class CSemantic: public PTVisitor
{
public:
    CSemantic();
    void visit(PTProgram *);
    void visit(PTBlock *);
    void visit(PTStatements *);
    void visit(PTDeclarationStatement *);
    void visit(PTWhileStatement *);
    void visit(PTIfStatement *);
    void visit(PTAssignStatement *);
    void visit(PTReadStatement *);
    void visit(PTWriteStatement *);
    void visit(PTModStatement *);
    void visit(PTDivStatement *);
    void visit(PTSqrtStatement *);

    void visit(PTAddExpression *);
    void visit(PTSubExpression *);
    void visit(PTMulExpression *);
    void visit(PTDivExpression *);

    void visit(PTNumberExpression *);
    void visit(PTFloatNumberExpression *);
    void visit(PTIdentifierExpression *);
    void visit(PTParenExpression *);

    void visit(PTOrExpression *);
    void visit(PTNotExpression *);
    void visit(PTNotEqualExpression *);
    void visit(PTMoreExpression *);
    void visit(PTMoreEqualExpression *);
    void visit(PTLogicParenExpression *);
    void visit(PTLessExpression *);
    void visit(PTLessEqualExpression *);
    void visit(PTEqualExpression *);
    void visit(PTAndExpression *);
    void visit(PTStringExpression *);

    void releaseTheTable();
    bool declareCheck(QString, VAR::VariableType);
    int typeCheck(QString, QString);
    bool toInt(QString);
    bool toFloat(QString);
    //bool toString(QString); //�������� �� �����������

private:
    //std::map<QString, int> _table;
    PTCharsTable _charTable;
};

#endif // CSEMANTIC_H
