#include "lexeme.h"

Lexeme::Lexeme(TID::Token token, QString &value, int line, int column)
    :_token(token)
    ,_value(value)
    ,_line(line)
    ,_column(column-value.size())
{

}

TID::Token Lexeme::token()
{
    return _token;
}

QString &Lexeme::value()
{
    return _value;
}

int Lexeme::line()
{
    return _line;
}

int Lexeme::column()
{
    return _column;
}
