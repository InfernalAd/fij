#ifndef LEXEME_H
#define LEXEME_H
#include "QString"


namespace TID
{
    enum Token
    {
        ADD,
        SUB,
        MUL,
        DIVIDE,
        LESS,
        MORE,
        LESS_OR_EQUAL,
        MORE_OR_EQUAL,
        LOGIC_EQUAL,
        NOT_EQUAL,
        EQUAL,
        AND,
        OR,
        NOT,

        LPAREN,
        RPAREN,
        LBRACE,
        RBRACE,

        STRING,
        NUMBER,
        FLOAT_NUMBER,
        IDENT,

        TYPE_INT,
        TYPE_FLOAT,
        TYPE_STRING,
        MOD,
        DIV,
        SQRT,

        READ,
        WRITE,
        WHILE,

        IF,
        ELSE,

        SEMICOLON,





        UNDEFINED,
        ENDOFFILE
    };
}


class Lexeme
{
public:
    Lexeme(TID::Token token, QString& value, int line=0,int column=0);

    TID::Token token();
    QString & value();
    int line();
    int column();
private:
    TID::Token _token;
    QString _value;
    int _line;
    int _column;
};

#endif // LEXEME_H
