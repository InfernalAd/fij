#include "lexemebuffer.h"

LexemeBuffer::LexemeBuffer()
{
    _currentPos = 0;
}

Lexeme & LexemeBuffer::peek()
{
    if (_lexemes.size() > _currentPos)
    {
        return _lexemes[_currentPos];
    }
    else
    {
        return Lexeme(TID::UNDEFINED,QString(""));
    }
}

Lexeme & LexemeBuffer::next()
{
    if (_lexemes.size() > _currentPos)
    {
        //qDebug() << "next" << _currentPos << _lexemes[_currentPos].token();
        _currentPos +=1;
        return _lexemes[_currentPos-1];
    }
    else
    {
        return Lexeme(TID::UNDEFINED,QString(""));
    }
}

int LexemeBuffer::size(){
    return _lexemes.size();
}

void LexemeBuffer::getDataFromLexer(Lexer * lex)
{
    Lexeme lexeme = lex->next();
    while (lexeme.token() != TID::UNDEFINED){
        qDebug() << lexeme.token() << lexeme.value();
        this->pushLexeme(lexeme);
        lexeme = lex->next();
    }
    this->seek(0);
}

void LexemeBuffer::seek(int pos)
{
    _currentPos = pos;
}

void LexemeBuffer::pushLexeme(Lexeme newLexeme)
{
    _lexemes.push_back(newLexeme);
}
