#ifndef LEXEMEBUFFER_H
#define LEXEMEBUFFER_H

#include "lexer.h"
#include "QVector"

class LexemeBuffer
{
public:
    LexemeBuffer();
    Lexeme &peek();
    Lexeme &next();
    void pushLexeme(Lexeme);
    int size();
    void getDataFromLexer(Lexer *lex);
    void seek(int);
private:
    std::vector<Lexeme> _lexemes;
    int _currentPos;

};

#endif // LEXEMEBUFFER_H
