#include "lexer.h"

Lexer::Lexer(std::iostream *input)
    :_input(input)
    ,_line(0)
    ,_column(0)
{
}

Lexeme Lexer::next()
{
    char ch = _input->peek();
    if (ch == -1){
        return Lexeme(TID::ENDOFFILE,QString("EOF"),_line,_column);
    }
    while (isDelim(ch)){
        this->getChar();
        ch = _input->peek();
    }
    switch (ch){
        case '+':
            this->getChar();
            return Lexeme(TID::ADD,QString("+"),_line,_column);
        break;
        case '-':
            this->getChar();
            return Lexeme(TID::SUB,QString("-"),_line,_column);
        break;
        case '*':
            this->getChar();
            return Lexeme(TID::MUL,QString("*"),_line,_column);
        break;
        case '/':
            this->getChar();
            return Lexeme(TID::DIVIDE,QString("/"),_line,_column);
        break;
        case '>':
            this->getChar();
            return Lexeme(TID::MORE,QString(">"),_line,_column);
        break;
        case '<':
            this->getChar();
            return Lexeme(TID::LESS,QString("<"),_line,_column);
        break;
        case ';':
            this->getChar();
            return Lexeme(TID::SEMICOLON,QString(";"),_line,_column);
        break;
        case '=':
        {
            this->getChar();
            char nextCh = _input->peek();
            switch (nextCh){
                case '=':
                    this->getChar();
                    return Lexeme(TID::LOGIC_EQUAL,QString("=="),_line,_column);
                break;
                case '>':
                    this->getChar();
                    return Lexeme(TID::MORE_OR_EQUAL,QString("=>"),_line,_column);
                break;
                case '<':
                    this->getChar();
                    return Lexeme(TID::LESS_OR_EQUAL,QString("=<"),_line,_column);
                break;
                default:
                    return Lexeme(TID::EQUAL,QString("="),_line,_column);
                break;
            }
        }
        break;
        case '&':
        {
            this->getChar();
            char nextCh = _input->peek();
            if (nextCh == '&'){
                this->getChar();
                return Lexeme(TID::AND,QString("&&"),_line,_column);
            }
            return Lexeme(TID::UNDEFINED,QString("&"),_line,_column);
        break;
        }
        case '|':
        {
            this->getChar();
            char nextCh = _input->peek();
            if (nextCh == '|'){
                this->getChar();
                return Lexeme(TID::OR,QString("||"),_line,_column);
            }
            return Lexeme(TID::UNDEFINED,QString("|"),_line,_column);
     }
        break;
        case '!':
        {
            this->getChar();
            char nextCh = _input->peek();
            if (nextCh == '='){
                this->getChar();
                return Lexeme(TID::NOT_EQUAL,QString("!="),_line,_column);
            }
            return Lexeme(TID::NOT,QString("!"),_line,_column);
        }
        break;
        case '(':
            this->getChar();
            return Lexeme(TID::LPAREN,QString("("),_line,_column);
        break;
        case ')':
            this->getChar();
            return Lexeme(TID::RPAREN,QString(")"),_line,_column);
        break;
        case '{':
            this->getChar();
            return Lexeme(TID::LBRACE,QString("{"),_line,_column);
        break;
        case '}':
            this->getChar();
            return Lexeme(TID::RBRACE,QString("}"),_line,_column);
        break;
        case '\"':
        {
            this->getChar();
            std::string value;
            char nextCh = this->getChar();
            while (nextCh != '\"'){
                value.push_back(nextCh);
                nextCh = this->getChar();
            }
            Lexeme reserved = this->checkReserved(value);
            if (reserved.token() != TID::UNDEFINED){
                return reserved;
            }
            return Lexeme(TID::STRING,QString::fromStdString(value),_line,_column);
        }
        break;
        default:
        {
            if (isIdentChar(ch)){//ident
                this->getChar();
                std::string value;
                value.push_back(ch);
                char nextCh = this->getChar();
                while (isIdentChar(nextCh)){
                    value.push_back(nextCh);
                    nextCh = _input->peek();
                    if (isIdentChar(nextCh)){
                        this->getChar();
                    }
                }
                Lexeme reserved = checkReserved(value);
                if (reserved.token() == TID::UNDEFINED){
                    return Lexeme(TID::IDENT,QString::fromStdString(value),_line,_column);
                }
                else{
                    return reserved;
                }
            }
            if ((ch >= '0')&&(ch <='9')){
                bool isFloat = false;
                bool isError = false;
                this->getChar();
                std::string value;
                value.push_back(ch);
                char nextCh = _input->peek();
                while ((isNumberChar(nextCh)) &&(!isError)){
                    this->getChar();
                    if (nextCh =='.'){
                        if (!isFloat){
                        isFloat = true;
                        }
                        else{
                            isError = true;
                        }
                    }
                    value.push_back(nextCh);

                    nextCh = _input->peek();
                    //this->getChar();

                }
                if (isError){
                    return Lexeme(TID::UNDEFINED,QString::fromStdString(value),_line,_column);
                }
                if (!isFloat){
                    return Lexeme(TID::NUMBER,QString::fromStdString(value),_line,_column);
                }
                else{
                    return Lexeme(TID::FLOAT_NUMBER,QString::fromStdString(value),_line,_column);
                }

            }

            return Lexeme(TID::UNDEFINED,QString(ch),_line,_column);
        }
        break;

    }
}


char Lexer::getChar(){
    char ch = _input->get();
    if (ch == '\n'){
        _line++;
        _column = 0;
    }
    else{
        _column++;
    }
    return ch;
}

bool Lexer::isDelim(char ch)
{
    return ((ch == ' ') || (ch == '\n') || (ch == '\t'));
}

bool Lexer::isIdentChar(char ch)
{
    return (((ch >='a') && (ch <= 'z')) || ((ch >='A') && (ch <= 'Z')) || (ch == '_'));
}

bool Lexer::isNumberChar(char ch)
{
    return ((ch >='0') && (ch <= '9'))||(ch=='.');
}

Lexeme Lexer::checkReserved(std::string value)
{
    if (value == "T_INT"){
        return Lexeme(TID::TYPE_INT,QString::fromStdString(value),_line,_column);
    }
    if (value == "T_FLOAT"){
        return Lexeme(TID::TYPE_FLOAT,QString::fromStdString(value),_line,_column);
    }
    if (value == "T_STRING"){
        return Lexeme(TID::TYPE_STRING,QString::fromStdString(value),_line,_column);
    }
    if (value == "MOD"){
        return Lexeme(TID::MOD,QString::fromStdString(value),_line,_column);
    }
    if (value == "DIV"){
        return Lexeme(TID::DIV,QString::fromStdString(value),_line,_column);
    }
    if (value == "SQRT"){
        return Lexeme(TID::SQRT,QString::fromStdString(value),_line,_column);
    }
    if (value == "READ"){
        return Lexeme(TID::READ,QString::fromStdString(value),_line,_column);
    }
    if (value == "WRITE"){
        return Lexeme(TID::WRITE,QString::fromStdString(value),_line,_column);
    }
    if (value == "WHILE"){
        return Lexeme(TID::WHILE,QString::fromStdString(value),_line,_column);
    }
    if (value == "IF"){
        return Lexeme(TID::IF,QString::fromStdString(value),_line,_column);
    }
    if (value == "ELSE"){
        return Lexeme(TID::ELSE,QString::fromStdString(value),_line,_column);
    }
    return Lexeme(TID::UNDEFINED,QString(""));
}
