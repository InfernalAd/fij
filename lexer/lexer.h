#ifndef LEXER_H
#define LEXER_H


#include "lexeme.h"
#include "istream"
#include "QDebug"
class Lexer
{
public:
    Lexer(std::iostream *);
    Lexeme next();


private:
    bool isDelim(char ch);
    bool isIdentChar(char ch);
    bool isNumberChar(char ch);

    Lexeme checkReserved(std::string value);
    char getChar();
private:
    std::iostream * _input;
    int _column;
    int _line;

};

#endif // LEXER_H
