#include <QCoreApplication>
#include "lexer/lexemebuffer.h"
#include "lexer/lexer.h"
#include "parsetree/nodes/ptstatements.h"
#include "parsetree/nodes/statements/ptdeclarationstatement.h"
#include "parsetree/ptnode.h"
#include "CSemantic.h"
#include "sstream"
#include "QDebug"

#include "codegenerator/codegenerator.h"


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    //CodeGenerator codegen;
    /*std::stringstream buf;
    buf << "{\nT_INT 1\n}";
    Lexer lex(&buf);
    Lexeme lexeme = lex.next();
    while (lexeme.token() != TID::ENDOFFILE){
        qDebug() << lexeme.value() << lexeme.line() << lexeme.column() << (lexeme.token() == TID::NUMBER);
        lexeme = lex.next();
    }*/

    /*PTProgram * prog = new PTProgram;
    PTBlock * block = new PTBlock;
    PTStatements * stmts = new PTStatements;
    PTIfStatement * ifstmt = new PTIfStatement;
    PTWhileStatement * whilestmt = new PTWhileStatement;
    PTStatements * thenstmt = new PTStatements;
    PTLessExpression * expr = new PTLessExpression;
    PTMoreExpression * expr2 = new PTMoreExpression;
    PTAndExpression * and = new PTAndExpression;

    PTWriteStatement * write = new PTWriteStatement;
    PTStringExpression * writeExpr = new PTStringExpression;
    writeExpr->setValue(QString("Hello World"));


    PTNumberExpression * num1 = new PTNumberExpression;
    PTNumberExpression * num2 = new PTNumberExpression;
    PTNumberExpression * num3 = new PTNumberExpression;
    PTNumberExpression * num4 = new PTNumberExpression;

    PTAddExpression * add = new PTAddExpression;
    PTAddExpression * add2 = new PTAddExpression;
    PTMulExpression * mul = new PTMulExpression;


    num1->setValue("1");
    num2->setValue("2");
    num3->setValue("3");
    num4->setValue("4");
    add->setLeftExpression(num1);
    add->setRightExpression(num2);
    add2->setLeftExpression(num3);
    add2->setRightExpression(num4);
    mul->setLeftExpression(add);
    mul->setRightExpression(add2);

    //codegen.parseExpression(mul);


    expr->setLeftExpression(add);
    expr->setRightExpression(num4);
    expr2->setLeftExpression(num1);
    expr2->setRightExpression(num2);
    and->setLeftRelation(expr);
    and->setRightRelation(expr2);
    write->setExpression(writeExpr);

    prog->setBlock(block);
    block->setStatements(stmts);
    stmts->addStatement(ifstmt);
    ifstmt->setLogicExpression(expr);
    ifstmt->setThenStatements(thenstmt);
    whilestmt->setExpression(expr);
    whilestmt->setStatements(thenstmt);
    thenstmt->addStatement(write);

    prog->accept(&codegen);*/

    CSemantic semantic;
    PTStatements * stmts = new PTStatements;
    PTDeclarationStatement * declare1 = new PTDeclarationStatement;
    PTDeclarationStatement * declare2 = new PTDeclarationStatement;
    PTDeclarationStatement * declare3 = new PTDeclarationStatement;
    PTDeclarationStatement * decalre4 = new PTDeclarationStatement;
    PTAssignStatement * assign1 = new PTAssignStatement;
    //PTExpression * expression = new PTExpression;
    PTIdentifierExpression * ident = new PTIdentifierExpression;

    declare1->setName("S1");
    declare1->setType(VAR::FLOAT);
    ident->setType(VAR::FLOAT);
    //assign1->setName("S1");

    declare3->setName("S1");
    declare3->setType(VAR::INT);

    decalre4->setName("");
    decalre4->setType(VAR::STRING);


    //assign1->setExpression();
    declare2->setName("S2");
    declare2->setType(VAR::UNKNOWN);


    //assign1->setName("S1");
    //assign1->setValue("123.56");

    stmts->addStatement(declare1);
    //stmts->addStatement(assign1);
    stmts->addStatement(declare2);
    stmts->addStatement(declare3);
    stmts->addStatement(decalre4);

    stmts->accept(&semantic);/*���������� ������������� ���������� � �����������*/
    semantic.releaseTheTable();

    return a.exec();
}
