#include "parser.h"

Parser::Parser()
{
}

void Parser::addState(ParserState * state)
{
    _states.insert(state->name(),state);
    connect(state,SIGNAL(jump(QString)),this,SLOT(onJumpRequest(QString)));
}

void Parser::onJumpRequest(QString stateName)
{
    if (_states.contains(stateName)){
        _currentState = _states.value(stateName);
    }
}

void Parser::onError(QString message)
{
    qDebug() << "error occured:" << message;
}
