#ifndef PARSER_H
#define PARSER_H
#include "QMap"
#include "parserstate.h"
#include "QObject"
#include "QDebug"

class Parser:public QObject
{
    Q_OBJECT
public:
    Parser();
    void addState(ParserState *);
private slots:
    void onJumpRequest(QString);
    void onError(QString);
private:
    QMap<QString,ParserState*> _states;
    ParserState * _currentState;
};

#endif // PARSER_H
