#include "parserstate.h"

ParserState::ParserState(QString name)
    :_name(name)
{
}

QString ParserState::name()
{
    return _name;
}

void ParserState::addJump(QString symbol, QString stateName)
{
    _jumps.insert(symbol,stateName);
}


void ParserState::addError(QString symbol, QString errorMessage)
{
    _errors.insert(symbol,errorMessage);
}

void ParserState::handle(QString symbol)
{
    if (_jumps.contains(symbol)){
        emit jump(_jumps.value(symbol));
    }
    else if (_errors.contains(symbol)){
        emit error(_errors.value(symbol));
    }
    else{
        emit error("Unexpected symbol " + symbol);
    }
}
