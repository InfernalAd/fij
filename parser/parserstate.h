#ifndef PARSERSTATE_H
#define PARSERSTATE_H
#include "QMap"
#include "QString"
#include "QObject"

class ParserState:public QObject
{
    Q_OBJECT
public:
    ParserState(QString name);

    QString name();
    void addJump(QString symbol,QString stateName);
    void addError(QString symbol,QString errorMessage);
    void handle(QString symbol);
signals:
    void jump(QString);
    void error(QString);
private:
    QString _name;
    QMap<QString,QString> _jumps;
    QMap<QString,QString> _errors;

};

#endif // PARSERSTATE_H
