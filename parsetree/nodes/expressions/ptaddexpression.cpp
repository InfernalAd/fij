#include "ptaddexpression.h"

PTAddExpression::PTAddExpression()
{
}

void PTAddExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

QString PTAddExpression::toString()
{
    QString res;
    res += this->leftExpression()->toString();
    res += "+";
    res += this->rightExpression()->toString();
    return res;
}

QString PTAddExpression::toPolishNotationString()
{
    QString res;
    res += this->leftExpression()->toPolishNotationString();
    res += this->rightExpression()->toPolishNotationString();
    res += "+";
    return res;
}
QVector<QString> PTAddExpression::toPolishNotation(){
    QVector<QString> res;
    res += this->leftExpression()->toPolishNotation();
    res += this->rightExpression()->toPolishNotation();
    res.push_back("+");
    return res;
}

VAR::VariableType PTAddExpression::getType()
{
    if((this->leftExpression()->getType() == VAR::INT) && (this->rightExpression()->getType() == VAR::INT))
    {
        return VAR::INT;
    }
    if((this->leftExpression()->getType() == VAR::FLOAT) && (this->rightExpression()->getType() == VAR::FLOAT))
    {
        return VAR::FLOAT;
    }
    if(((this->leftExpression()->getType() == VAR::INT) && (this->rightExpression()->getType() == VAR::FLOAT)) || ((this->leftExpression()->getType() == VAR::FLOAT) && (this->rightExpression()->getType() == VAR::INT)))
    {
        return VAR::FLOAT;
    }
    if((this->leftExpression()->getType() == VAR::STRING) && (this->rightExpression()->getType() == VAR::STRING))
    {
        return VAR::STRING;
    }
    if(((this->leftExpression()->getType() == VAR::INT) && (this->rightExpression()->getType() == VAR::STRING)) || ((this->leftExpression()->getType() == VAR::STRING) && (this->rightExpression()->getType() == VAR::INT)))
    {
        return VAR::UNKNOWN;
    }
    if(((this->leftExpression()->getType() == VAR::FLOAT) && (this->rightExpression()->getType() == VAR::STRING)) || ((this->leftExpression()->getType() == VAR::STRING) && (this->rightExpression()->getType() == VAR::FLOAT)))
    {
        return VAR::UNKNOWN;
    }
    return VAR::UNKNOWN;
}

/*string string   = string

int string      = unknown �������
string int      = unknown

string float    = unknown �������
float string    = unknown

unknown int     = unknown �������
int unknown     = unknown

unknown float   = unknown ���� ���� unknown
float unknown   = unknown

unknown string  = unknown ���� ���� unknowb
string unknown  = unknown

unknown unknown = unknown*/
