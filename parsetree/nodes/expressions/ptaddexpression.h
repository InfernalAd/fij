#ifndef PTADDEXPRESSION_H
#define PTADDEXPRESSION_H

#include "../ptbinaryexpression.h"

class PTAddExpression:public PTBinaryExpression
{
public:
    PTAddExpression();
    void accept(PTVisitor *);
    QString toString();
    VAR::VariableType getType();
    QString toPolishNotationString();
    QVector<QString> toPolishNotation();
};

#endif // PTADDEXPRESSION_H
