#ifndef PTDIVEXPRESSION_H
#define PTDIVEXPRESSION_H

#include "../ptbinaryexpression.h"

class PTDivExpression:public PTBinaryExpression
{
public:
    PTDivExpression();
    void accept(PTVisitor *);

    QString toString();

    VAR::VariableType getType();
    QString toPolishNotationString();
    QVector<QString> toPolishNotation();
};

#endif // PTDIVEXPRESSION_H
