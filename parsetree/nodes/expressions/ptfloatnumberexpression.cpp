#include "ptfloatnumberexpression.h"

PTFloatNumberExpression::PTFloatNumberExpression()
{
}


void PTFloatNumberExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

QString PTFloatNumberExpression::value()
{
    return _value;
}

void PTFloatNumberExpression::setValue(QString &val)
{
    _value = val;
}

QString PTFloatNumberExpression::toString()
{
    return _value;
}

QString PTFloatNumberExpression::toPolishNotationString()
{
    return _value;
}

QVector<QString> PTFloatNumberExpression::toPolishNotation(){
    QVector<QString> res;
    res.push_back(_value);
    return res;
}
