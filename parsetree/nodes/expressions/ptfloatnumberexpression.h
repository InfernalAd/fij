#ifndef PTFLOATNUMBEREXPRESSION_H
#define PTFLOATNUMBEREXPRESSION_H
#include "../ptexpression.h"

class PTFloatNumberExpression:public PTExpression
{
public:
    PTFloatNumberExpression();
    void accept(PTVisitor *);
    QString value();
    void setValue(QString &val);

    QString toString();
    VAR::VariableType getType() {return VAR::FLOAT;}
    QString toPolishNotationString();
    QVector<QString> toPolishNotation();
private:
    QString _value;
};

#endif // PTFLOATNUMBEREXPRESSION_H
