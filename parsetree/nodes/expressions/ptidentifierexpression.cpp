#include "ptidentifierexpression.h"

PTIdentifierExpression::PTIdentifierExpression()
{
}

void PTIdentifierExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

void PTIdentifierExpression::setType(VAR::VariableType type)
{
    _type = type;
}

VAR::VariableType PTIdentifierExpression::getType()
{
    return _type;
}

QString PTIdentifierExpression::toPolishNotationString()
{
    return _value;
}

QVector<QString> PTIdentifierExpression::toPolishNotation()
{
    QVector<QString> res;
    res.push_back(_value);
    return res;
}

QString PTIdentifierExpression::toString()
{
    return _value;
}
