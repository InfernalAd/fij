#ifndef PTIDENTIFIEREXPRESSION_H
#define PTIDENTIFIEREXPRESSION_H
#include "../ptexpression.h"


class PTIdentifierExpression:public PTExpression
{
public:
    PTIdentifierExpression();
    void accept(PTVisitor *visitor);
    void setType(VAR::VariableType);
    VAR::VariableType getType();
    QString toPolishNotationString();
    QVector<QString> toPolishNotation();
    QString toString();
    bool isSimple(){return true;}
private:
    QString _value;
    VAR::VariableType _type;
};

#endif // PTIDENTIFIEREXPRESSION_H
