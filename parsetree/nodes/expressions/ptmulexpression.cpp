#include "ptmulexpression.h"

PTMulExpression::PTMulExpression()
{
}

void PTMulExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

QString PTMulExpression::toString()
{
    QString res;
    res += this->leftExpression()->toString();
    res += "*";
    res += this->rightExpression()->toString();
    return res;
}

QString PTMulExpression::toPolishNotationString()
{
    QString res;
    res += this->leftExpression()->toPolishNotationString();
    res += this->rightExpression()->toPolishNotationString();
    res += "*";
    return res;
}

QVector<QString> PTMulExpression::toPolishNotation(){
    QVector<QString> res;
    res += this->leftExpression()->toPolishNotation();
    res += this->rightExpression()->toPolishNotation();
    res.push_back("*");
    return res;
}

VAR::VariableType PTMulExpression::getType()
{
    if((this->leftExpression()->getType() == VAR::INT) && (this->rightExpression()->getType() == VAR::INT))
    {
        return VAR::INT;
    }
    if((this->leftExpression()->getType() == VAR::FLOAT) && (this->rightExpression()->getType() == VAR::FLOAT))
    {
        return VAR::FLOAT;
    }
    if(((this->leftExpression()->getType() == VAR::INT) && (this->rightExpression()->getType() == VAR::FLOAT)) || ((this->leftExpression()->getType() == VAR::FLOAT) && (this->rightExpression()->getType() == VAR::INT)))
    {
        return VAR::FLOAT;
    }
    if((this->leftExpression()->getType() == VAR::STRING) && (this->rightExpression()->getType() == VAR::STRING))
    {
        return VAR::STRING;
    }
    if(((this->leftExpression()->getType() == VAR::INT) && (this->rightExpression()->getType() == VAR::STRING)) || ((this->leftExpression()->getType() == VAR::STRING) && (this->rightExpression()->getType() == VAR::INT)))
    {
        return VAR::UNKNOWN;
    }
    if(((this->leftExpression()->getType() == VAR::FLOAT) && (this->rightExpression()->getType() == VAR::STRING)) || ((this->leftExpression()->getType() == VAR::STRING) && (this->rightExpression()->getType() == VAR::FLOAT)))
    {
        return VAR::UNKNOWN;
    }
    return VAR::UNKNOWN;
}
