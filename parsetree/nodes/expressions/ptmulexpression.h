#ifndef PTMULEXPRESSION_H
#define PTMULEXPRESSION_H

#include "../ptbinaryexpression.h"

class PTMulExpression:public PTBinaryExpression
{
public:
    PTMulExpression();
    void accept(PTVisitor *);
    QString toString();
    VAR::VariableType getType();
    QString toPolishNotationString();
    QVector<QString> toPolishNotation();
};

#endif // PTMULEXPRESSION_H
