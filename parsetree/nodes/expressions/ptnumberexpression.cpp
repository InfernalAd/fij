#include "ptnumberexpression.h"

PTNumberExpression::PTNumberExpression()
{
}

void PTNumberExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

QString PTNumberExpression::value()
{
    return _value;
}

void PTNumberExpression::setValue(QString val)
{
    _value = val;
}

QString PTNumberExpression::toString()
{
    return _value;
}

QString PTNumberExpression::toPolishNotationString()
{
    return _value;
}

QVector<QString> PTNumberExpression::toPolishNotation()
{
    QVector<QString> res;
    res.push_back(_value);
    return res;
}
