#ifndef PTNUMBEREXPRESSION_H
#define PTNUMBEREXPRESSION_H

#include "../ptexpression.h"

class PTNumberExpression:public PTExpression
{
public:
    PTNumberExpression();
    void accept(PTVisitor *);

    QString value();
    void setValue(QString);

    QString toString();
    QString toPolishNotationString();
    QVector<QString> toPolishNotation();
    bool isSimple(){return true;}
    VAR::VariableType getType(){return VAR::INT;}
private:
    QString _value;

};

#endif // PTNUMBEREXPRESSION_H
