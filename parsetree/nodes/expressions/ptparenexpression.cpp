#include "ptparenexpression.h"

PTParenExpression::PTParenExpression()
{
}

void PTParenExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

PTExpression *PTParenExpression::expression()
{
    return _expression;
}

void PTParenExpression::setExpression(PTExpression * expr)
{
    _expression = expr;
}

QString PTParenExpression::toString()
{
    return QString("(" + _expression->toString() + ")");
}

QString PTParenExpression::toPolishNotationString()
{
    return QString("(" + _expression->toPolishNotationString() + ")");
}
