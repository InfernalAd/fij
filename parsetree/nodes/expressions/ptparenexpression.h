#ifndef PTPARENEXPRESSION_H
#define PTPARENEXPRESSION_H
#include "../ptexpression.h"

class PTParenExpression:public PTExpression
{
public:
    PTParenExpression();
    void accept(PTVisitor *);
    PTExpression * expression();
    void setExpression(PTExpression *);

    QString toString();
    QString toPolishNotationString();
private:
    PTExpression * _expression;
};

#endif // PTPARENEXPRESSION_H
