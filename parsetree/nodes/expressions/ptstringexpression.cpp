#include "ptstringexpression.h"

PTStringExpression::PTStringExpression()
{
}


QString PTStringExpression::toString()
{
    return _value;
}


QVector<QString> PTStringExpression::toPolishNotation()
{
    QVector<QString> res;
    res.push_back(_value);
    return res;
}


QString PTStringExpression::toPolishNotationString()
{
    return _value;
}


void PTStringExpression::setValue(QString val)
{
    _value = val;
}


void PTStringExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}
