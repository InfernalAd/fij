#ifndef PTSTRINGEXPRESSTION_H
#define PTSTRINGEXPRESSTION_H

#include "../ptexpression.h"

class PTStringExpression:public PTExpression
{
public:
    PTStringExpression();
    void accept(PTVisitor *);
    void setValue(QString);
    QString toString();
    QVector<QString> toPolishNotation();
    QString toPolishNotationString();
    VAR::VariableType getType(){return VAR::STRING;}
    bool isSimple(){return true;};
private:
    QString _value;
};

#endif // PTSTRINGEXPRESSTION_H
