#ifndef PTSUBEXPRESSION_H
#define PTSUBEXPRESSION_H
#include "../ptbinaryexpression.h"

class PTSubExpression:public PTBinaryExpression
{
public:
    PTSubExpression();
    void accept(PTVisitor *);
    QString toString();
    VAR::VariableType getType();
    QString toPolishNotationString();
    QVector<QString> toPolishNotation();
};

#endif // PTSUBEXPRESSION_H
