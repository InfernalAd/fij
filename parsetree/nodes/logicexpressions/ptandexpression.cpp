#include "ptandexpression.h"

PTAndExpression::PTAndExpression()
    :_leftRelation(NULL)
    ,_rightRelation(NULL)
{
}

PTAndExpression::~PTAndExpression()
{
    delete _leftRelation;
    delete _rightRelation;
}

void PTAndExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

QString PTAndExpression::toString()
{
    if (_leftRelation == NULL){
        return "Error: Left relation is NULL";
    }
    if (_rightRelation == NULL){
        return "Error: Right relation is NULL";
    }

    QString res;
    res += _leftRelation->toString();
    res += " && ";
    res += _rightRelation->toString();
    return res;
}


PTLogicRelation *PTAndExpression::leftExpr()
{
    return _leftRelation;
}

PTLogicRelation *PTAndExpression::rightExpr()
{
    return _rightRelation;
}

void PTAndExpression::setLeftRelation(PTLogicRelation * rel)
{
    _leftRelation = rel;
}

void PTAndExpression::setRightRelation(PTLogicRelation * rel)
{
    _rightRelation = rel;
}
