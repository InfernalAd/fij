#ifndef PTANDEXPRESSION_H
#define PTANDEXPRESSION_H
#include "../ptlogicexpression.h"
#include "../ptlogicrelation.h"

class PTAndExpression:public PTLogicExpression
{
public:
    PTAndExpression();
    ~PTAndExpression();
    void accept(PTVisitor *);
    QString toString();

    PTLogicRelation * leftExpr();
    PTLogicRelation * rightExpr();
    void setLeftRelation(PTLogicRelation *);
    void setRightRelation(PTLogicRelation *);
private:
    PTLogicRelation * _leftRelation;
    PTLogicRelation * _rightRelation;
};

#endif // PTANDEXPRESSION_H
