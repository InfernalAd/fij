#include "ptequalexpression.h"

PTEqualExpression::PTEqualExpression()
{
}

void PTEqualExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

QString PTEqualExpression::toString()
{
    return QString(" == ");
}
