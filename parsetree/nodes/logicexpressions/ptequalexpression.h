#ifndef PTEQUALEXPRESSION_H
#define PTEQUALEXPRESSION_H
#include "../ptlogicrelation.h"

class PTEqualExpression:public PTLogicRelation
{
public:
    PTEqualExpression();
    void accept(PTVisitor *visitor);
    QString toString();
};

#endif // PTEQUALEXPRESSION_H
