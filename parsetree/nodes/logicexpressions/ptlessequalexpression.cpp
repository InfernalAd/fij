#include "ptlessequalexpression.h"

PTLessEqualExpression::PTLessEqualExpression()
{
}

void PTLessEqualExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

QString PTLessEqualExpression::toString()

{
    return QString(" =< ");
}
