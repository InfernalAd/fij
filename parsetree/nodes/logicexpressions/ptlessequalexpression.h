#ifndef PTLESSEQUALEXPRESSION_H
#define PTLESSEQUALEXPRESSION_H
#include "../ptlogicrelation.h"


class PTLessEqualExpression:public PTLogicRelation
{
public:
    PTLessEqualExpression();
    void accept(PTVisitor *);
    QString toString();
};

#endif // PTLESSEQUALEXPRESSION_H
