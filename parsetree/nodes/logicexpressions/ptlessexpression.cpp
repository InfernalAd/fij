#include "ptlessexpression.h"

PTLessExpression::PTLessExpression()
{
}

void PTLessExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

QString PTLessExpression::toString()
{
    return QString("eax<ebx");
}
