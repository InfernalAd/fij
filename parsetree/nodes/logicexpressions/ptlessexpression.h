#ifndef PTLESSEXPRESSION_H
#define PTLESSEXPRESSION_H
#include "../ptlogicrelation.h"


class PTLessExpression:public PTLogicRelation
{
public:
    PTLessExpression();
    void accept(PTVisitor *);
    QString toString();
};

#endif // PTLESSEXPRESSION_H
