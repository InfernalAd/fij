#ifndef PTPARENEXPRESSION_H
#define PTPARENEXPRESSION_H
#include "../ptlogicexpression.h"


class PTLogicParenExpression:public PTLogicExpression
{
public:
    PTLogicParenExpression();
    void accept(PTVisitor *);
};

#endif // PTPARENEXPRESSION_H
