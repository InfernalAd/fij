#ifndef PTMOREEQUALEXPRESSION_H
#define PTMOREEQUALEXPRESSION_H
#include "../ptlogicrelation.h"


class PTMoreEqualExpression:public PTLogicRelation
{
public:
    PTMoreEqualExpression();
    void accept(PTVisitor *);
    QString toString();

};

#endif // PTMOREEQUALEXPRESSION_H
