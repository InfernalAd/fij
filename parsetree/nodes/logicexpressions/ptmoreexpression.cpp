#include "ptmoreexpression.h"

PTMoreExpression::PTMoreExpression()
{
}

void PTMoreExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

QString PTMoreExpression::toString()
{
    return QString("eax>ebx");
}
