#ifndef PTMOREEXPRESSION_H
#define PTMOREEXPRESSION_H
#include "../ptlogicrelation.h"


class PTMoreExpression:public PTLogicRelation
{
public:
    PTMoreExpression();
    void accept(PTVisitor *);
    QString toString();
};

#endif // PTMOREEXPRESSION_H
