#include "ptnotequalexpression.h"

PTNotEqualExpression::PTNotEqualExpression()
{
}

void PTNotEqualExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

QString PTNotEqualExpression::toString()
{
    return QString(" != ");
}
