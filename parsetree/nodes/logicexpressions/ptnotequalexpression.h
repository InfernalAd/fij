#ifndef PTNOTEQUALEXPRESSION_H
#define PTNOTEQUALEXPRESSION_H
#include "../ptlogicrelation.h"


class PTNotEqualExpression:public PTLogicRelation
{
public:
    PTNotEqualExpression();
    void accept(PTVisitor *);
    QString toString();
};

#endif // PTNOTEQUALEXPRESSION_H
