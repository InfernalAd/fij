#include "ptnotexpression.h"

PTNotExpression::PTNotExpression()
    :_relation(NULL)
{
}

void PTNotExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

QString PTNotExpression::toString()
{
    if (_relation == NULL){
        return "relation is NULL";
    }
    QString res;
    res += "!";
    res += _relation->toString();
    return res;
}

PTLogicRelation *PTNotExpression::relation()
{
    return _relation;
}


void PTNotExpression::setRelation(PTLogicRelation * rel)
{
    _relation = rel;
}
