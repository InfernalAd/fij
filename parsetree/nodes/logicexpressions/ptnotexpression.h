#ifndef PTNOTEXPRESSION_H
#define PTNOTEXPRESSION_H
#include "../ptlogicexpression.h"
#include "../ptlogicrelation.h"


class PTNotExpression:public PTLogicExpression
{
public:
    PTNotExpression();
    void accept(PTVisitor *);
    QString toString();

    PTLogicRelation * relation();
    void setRelation(PTLogicRelation*);
private:
    PTLogicRelation * _relation;
};

#endif // PTNOTEXPRESSION_H
