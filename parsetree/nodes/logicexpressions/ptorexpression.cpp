#include "ptorexpression.h"

PTOrExpression::PTOrExpression()
{
}

void PTOrExpression::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

PTOrExpression::~PTOrExpression()
{
    delete _leftRelation;
    delete _rightRelation;
}

QString PTOrExpression::toString()
{
    if (_leftRelation == NULL){
        return "Error: Left relation is NULL";
    }
    if (_rightRelation == NULL){
        return "Error: Right relation is NULL";
    }

    QString res;
    res += _leftRelation->toString();
    res += " || ";
    res += _rightRelation->toString();
    return res;
}


PTLogicRelation *PTOrExpression::leftExpr()
{
    return _leftRelation;
}

PTLogicRelation *PTOrExpression::rightExpr()
{
    return _rightRelation;
}

void PTOrExpression::setLeftRelation(PTLogicRelation * rel)
{
    _leftRelation = rel;
}

void PTOrExpression::setRightRelation(PTLogicRelation * rel)
{
    _rightRelation = rel;
}
