#ifndef PTOREXPRESSION_H
#define PTOREXPRESSION_H
#include "../ptlogicexpression.h"
#include "../ptlogicrelation.h"

class PTOrExpression:public PTLogicExpression
{
public:
    PTOrExpression();
    ~PTOrExpression();
    void accept(PTVisitor *);
    QString toString();

    PTLogicRelation * leftExpr();
    PTLogicRelation * rightExpr();
    void setLeftRelation(PTLogicRelation *);
    void setRightRelation(PTLogicRelation *);
private:
    PTLogicRelation * _leftRelation;
    PTLogicRelation * _rightRelation;
};

#endif // PTOREXPRESSION_H
