#ifndef NODES_H
#define NODES_H

#include "ptprogram.h"
#include "ptblock.h"
#include "ptstatements.h"
#include "statements/ptdeclarationstatement.h"
#include "statements/ptwhilestatement.h"
#include "statements/ptifstatement.h"
#include "statements/ptassignstatement.h"
#include "statements/ptreadstatement.h"
#include "statements/ptwritestatement.h"
#include "statements/ptmodstatement.h"
#include "statements/ptdivstatement.h"
#include "statements/ptsqrtstatement.h"

#include "expressions/ptaddexpression.h"
#include "expressions/ptdivexpression.h"
#include "expressions/ptfloatnumberexpression.h"
#include "expressions/ptidentifierexpression.h"
#include "expressions/ptmulexpression.h"
#include "expressions/ptnumberexpression.h"
#include "expressions/ptparenexpression.h"
#include "expressions/ptsubexpression.h"
#include "expressions/ptstringexpression.h"

#include "logicexpressions/ptandexpression.h"
#include "logicexpressions/ptequalexpression.h"
#include "logicexpressions/ptlessequalexpression.h"
#include "logicexpressions/ptlessexpression.h"
#include "logicexpressions/ptlogicparenexpression.h"
#include "logicexpressions/ptmoreequalexpression.h"
#include "logicexpressions/ptmoreexpression.h"
#include "logicexpressions/ptnotequalexpression.h"
#include "logicexpressions/ptnotexpression.h"
#include "logicexpressions/ptorexpression.h"



#endif // NODES_H
