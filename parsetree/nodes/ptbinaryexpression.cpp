#include "ptbinaryexpression.h"

PTBinaryExpression::PTBinaryExpression()
    :_leftExpression(NULL)
    ,_rightExpression(NULL)
{
}

PTBinaryExpression::~PTBinaryExpression()
{
    delete _leftExpression;
    delete _rightExpression;
}

PTExpression *PTBinaryExpression::leftExpression()
{
    return _leftExpression;
}

PTExpression *PTBinaryExpression::rightExpression()
{
    return _rightExpression;
}

void PTBinaryExpression::setLeftExpression(PTExpression * expr)
{
    _leftExpression = expr;
}

void PTBinaryExpression::setRightExpression(PTExpression * expr)
{
    _rightExpression = expr;
}

/*�� �������
  �������� ��� ����� ����������
  string string   = string

  int string      = unknown
  string int      = unknown

  string float    = unknown
  float string    = unknown

  unknown int     = unknown
  int unknown     = unknown

  unknown float   = unknown
  float unknown   = unknown

  unknown string  = unknown
  string unknown  = unknown

  unknown unknown = unknown
*/
//VAR::VariableType PTBinaryExpression::getType()
//{
    //���������� ������������
    /*int int = int
    if((_leftExpression->getType() == VAR::INT) && (_rightExpression->getType() == VAR::INT))
    {
        return VAR::INT;
    }
    /*int float = float
    if((_leftExpression->getType() == VAR::FLOAT) && (_rightExpression->getType() == VAR::FLOAT))
    {
        return VAR::FLOAT;
    }
    /*float int = float
    if(((_leftExpression->getType() == VAR::INT) && (_rightExpression->getType() == VAR::FLOAT)) || ((_leftExpression->getType() == VAR::FLOAT) && (_rightExpression->getType() == VAR::INT)))
    {
        return VAR::FLOAT;
    }*/
//}

bool PTBinaryExpression::isSimple()
{
    return false;
}
