#ifndef PTBINARYEXPRESSION_H
#define PTBINARYEXPRESSION_H
#include "ptexpression.h"

class PTBinaryExpression:public PTExpression
{
public:
    PTBinaryExpression();
    virtual ~PTBinaryExpression();
    PTExpression * leftExpression();
    PTExpression * rightExpression();

    void setLeftExpression(PTExpression *);
    void setRightExpression(PTExpression *);

    //VAR::VariableType getType();

    bool isSimple();
private:
    PTExpression * _leftExpression;
    PTExpression * _rightExpression;
};

#endif // PTBINARYEXPRESSION_H
