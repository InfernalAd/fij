#include "ptblock.h"

PTBlock::PTBlock()
    :_statements(0)
{
}

void PTBlock::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

void PTBlock::setStatements(PTStatements * statements)
{
    _statements = statements;
}


PTStatements* PTBlock::statements()
{
    return _statements;
}
