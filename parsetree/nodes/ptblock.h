#ifndef PTBLOCK_H
#define PTBLOCK_H

#include "../ptnode.h"
#include "ptstatements.h"


class PTBlock:public PTNode
{
public:
    PTBlock();
    void accept(PTVisitor *);
    void setStatements(PTStatements *);
    PTStatements* statements();
private:
    PTStatements * _statements;
};

#endif // PTBLOCK_H
