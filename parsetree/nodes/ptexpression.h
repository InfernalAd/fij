#ifndef PTEXPRESSION_H
#define PTEXPRESSION_H
#include "../ptnode.h"
#include "QString"
#include "QVector"

class PTExpression:public PTNode
{
public:
    virtual QString toString()=0;
    virtual VAR::VariableType getType()=0;
    virtual QString toPolishNotationString()=0;
    virtual QVector<QString> toPolishNotation()=0;
    virtual bool isSimple()=0;
};

#endif // PTEXPRESSION_H
