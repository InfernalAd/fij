#ifndef PTLOGICEXPRESSION_H
#define PTLOGICEXPRESSION_H
#include "../ptnode.h"
#include "QString"

class PTLogicExpression:public PTNode
{
public:
    virtual QString toString()=0;
};

#endif // PTLOGICEXPRESSION_H
