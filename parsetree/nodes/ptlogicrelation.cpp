#include "ptlogicrelation.h"

PTLogicRelation::PTLogicRelation()
    :_leftExpression(NULL)
    ,_rightExpression(NULL)
{
}

PTLogicRelation::~PTLogicRelation()
{
    delete _leftExpression;
    delete _rightExpression;
}

PTExpression *PTLogicRelation::leftExpr()
{
    return _leftExpression;
}

PTExpression *PTLogicRelation::rightExpr()
{
    return _rightExpression;
}

void PTLogicRelation::setLeftExpression(PTExpression * expr)
{
    _leftExpression = expr;
}

void PTLogicRelation::setRightExpression(PTExpression * expr)
{
    _rightExpression = expr;
}
