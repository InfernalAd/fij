#ifndef PTLOGICRELATION_H
#define PTLOGICRELATION_H
#include "ptexpression.h"
#include "ptlogicexpression.h"
#include "QString"

class PTLogicRelation:public PTLogicExpression
{
public:
    PTLogicRelation();
    ~PTLogicRelation();
    PTExpression * leftExpr();
    PTExpression * rightExpr();
    void setLeftExpression(PTExpression *);
    void setRightExpression(PTExpression *);

    virtual QString toString()=0;
private:
    PTExpression * _leftExpression;
    PTExpression * _rightExpression;
};

#endif // PTLOGICRELATION_H
