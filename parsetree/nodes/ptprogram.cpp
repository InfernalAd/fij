#include "ptprogram.h"

PTProgram::PTProgram()
    :_block(0)
{
}

void PTProgram::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

void PTProgram::setBlock(PTBlock * block)
{
    _block = block;
}

PTBlock *PTProgram::block()
{
    return _block;
}
