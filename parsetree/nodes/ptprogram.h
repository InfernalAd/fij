#ifndef PTPROGRAM_H
#define PTPROGRAM_H

#include "../ptnode.h"
#include "ptblock.h"


class PTProgram:public PTNode
{
public:
    PTProgram();
    void accept(PTVisitor *);

    void setBlock(PTBlock *);
    PTBlock * block();
private:
    PTBlock * _block;

};

#endif // PTPROGRAM_H
