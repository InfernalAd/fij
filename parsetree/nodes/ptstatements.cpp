#include "ptstatements.h"

PTStatements::PTStatements()
{
}

void PTStatements::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

void PTStatements::addStatement(PTStatement * statement)
{
    _statements.push_back(statement);
}

int PTStatements::statementsNum()
{
    return _statements.size();
}


PTStatement *PTStatements::statement(int index)
{
    if (_statements.size() > index){
        return _statements[index];
    }
    return 0;
}
