#ifndef PTSTATEMENTS_H
#define PTSTATEMENTS_H
#include "../ptnode.h"
#include "vector"
#include "ptstatement.h"

class PTStatements:public PTNode
{
public:
    PTStatements();
    void accept(PTVisitor *);
    void addStatement(PTStatement *);
    int statementsNum();
    PTStatement * statement(int index);
private:
    std::vector<PTStatement *> _statements;
};

#endif // PTSTATEMENTS_H
