#ifndef PTWRITEEXPRESSION_H
#define PTWRITEEXPRESSION_H

#include "../ptnode.h"
#include "QString"
class PTWriteExpression:public PTNode{
public:
    virtual bool isIdent()=0;
    virtual QString toString()=0;
};

#endif // PTWRITEEXPRESSION_H
