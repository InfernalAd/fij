#include "ptassignstatement.h"

PTAssignStatement::PTAssignStatement()
{
}

void PTAssignStatement::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

void PTAssignStatement::setExpression(PTExpression * expression)
{
    _expression = expression;
}

PTExpression *PTAssignStatement::expression()
{
    return _expression;
}

QString PTAssignStatement::name()
{
    return _name;
}

void PTAssignStatement::setName(QString name)
{
    _name = name;
}
