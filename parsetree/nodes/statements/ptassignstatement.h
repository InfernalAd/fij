#ifndef PTASSIGNSTATEMENT_H
#define PTASSIGNSTATEMENT_H
#include "../ptstatement.h"
#include "parsetree/nodes/ptexpression.h"


class PTAssignStatement:public PTStatement
{
public:
    PTAssignStatement();
    void accept(PTVisitor *);

    void setExpression(PTExpression *);
    PTExpression * expression();

    QString name();
    void setName(QString);

private:
    QString _name;
    PTExpression * _expression;
};

#endif // PTASSIGNSTATEMENT_H
