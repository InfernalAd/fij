#include "ptdeclarationstatement.h"

PTDeclarationStatement::PTDeclarationStatement()
{
}

void PTDeclarationStatement::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

void PTDeclarationStatement::setType(VAR::VariableType type)
{
    _type = type;
}

void PTDeclarationStatement::setName(QString name)
{
    _name = name;
}

VAR::VariableType PTDeclarationStatement::type()
{
    return _type;
}

QString PTDeclarationStatement::name()
{
    return _name;
}
