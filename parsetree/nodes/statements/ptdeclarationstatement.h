#ifndef PTDECLARATIONSTATEMENT_H
#define PTDECLARATIONSTATEMENT_H
#include "../ptstatement.h"


class PTDeclarationStatement:public PTStatement
{
public:
    PTDeclarationStatement();
    void accept(PTVisitor *);

    void setType(VAR::VariableType);

    QString name();
    VAR::VariableType type();
    void setName(QString);

private:
    VAR::VariableType _type;
    QString _name;
};

#endif // PTDECLARATIONSTATEMENT_H
