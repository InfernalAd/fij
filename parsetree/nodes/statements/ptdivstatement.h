#ifndef PTDIVSTATEMENT_H
#define PTDIVSTATEMENT_H
#include "../ptstatement.h"


class PTDivStatement:public PTStatement
{
public:
    PTDivStatement();
    void accept(PTVisitor *visitor);
};

#endif // PTDIVSTATEMENT_H
