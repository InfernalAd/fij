#include "ptifstatement.h"

PTIfStatement::PTIfStatement()
    :_thenStatement(NULL)
    ,_elseStatement(NULL)
{
}

void PTIfStatement::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

void PTIfStatement::setLogicExpression(PTLogicExpression * expr)
{
    _expression = expr;
}

PTLogicExpression *PTIfStatement::expression()
{
    return _expression;
}

PTStatements *PTIfStatement::thenStatements()
{
    return _thenStatement;
}

PTStatements *PTIfStatement::elseStatements()
{
    return _elseStatement;
}

void PTIfStatement::setThenStatements(PTStatements * stmt)
{
    _thenStatement = stmt;
}

void PTIfStatement::setElseStatements(PTStatements * stmt)
{
    _elseStatement = stmt;
}
