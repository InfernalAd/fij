#ifndef PTIFSTATEMENT_H
#define PTIFSTATEMENT_H
#include "../ptstatement.h"
#include "../ptlogicexpression.h"

class PTIfStatement:public PTStatement
{
public:
    PTIfStatement();
    void accept(PTVisitor *);


    void setLogicExpression(PTLogicExpression *);
    PTLogicExpression * expression();

    PTStatements * thenStatements();
    PTStatements * elseStatements();
    void setThenStatements(PTStatements *);
    void setElseStatements(PTStatements *);
private:
    PTStatements * _thenStatement;
    PTStatements * _elseStatement;
    PTLogicExpression * _expression;
};

#endif // PTIFSTATEMENT_H
