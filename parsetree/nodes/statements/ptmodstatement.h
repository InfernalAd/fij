#ifndef PTMODSTATEMENT_H
#define PTMODSTATEMENT_H
#include "../ptstatement.h"


class PTModStatement:public PTStatement
{
public:
    PTModStatement();
    void accept(PTVisitor *);
};

#endif // PTMODSTATEMENT_H
