#ifndef PTREADSTATEMENT_H
#define PTREADSTATEMENT_H
#include "../ptstatement.h"


class PTReadStatement:public PTStatement
{
public:
    PTReadStatement();
    void accept(PTVisitor *);
};

#endif // PTREADSTATEMENT_H
