#ifndef PTSQRTSTATEMENT_H
#define PTSQRTSTATEMENT_H
#include "../ptstatement.h"


class PTSqrtStatement:public PTStatement
{
public:
    PTSqrtStatement();
    void accept(PTVisitor *);
};

#endif // PTSQRTSTATEMENT_H
