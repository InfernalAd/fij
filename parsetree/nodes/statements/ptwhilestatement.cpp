#include "ptwhilestatement.h"

PTWhileStatement::PTWhileStatement()
{
}

void PTWhileStatement::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

PTLogicExpression *PTWhileStatement::expression()
{
    return _expression;
}

PTStatements *PTWhileStatement::statements()
{
    return _statements;
}

void PTWhileStatement::setExpression(PTLogicExpression * expr)
{
    _expression = expr;
}

void PTWhileStatement::setStatements(PTStatements * stmts)
{
    _statements = stmts;
}
