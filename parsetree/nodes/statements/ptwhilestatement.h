#ifndef PTWHILESTATEMENT_H
#define PTWHILESTATEMENT_H
#include "../ptstatement.h"
#include "../ptlogicexpression.h"

class PTWhileStatement:public PTStatement
{
public:
    PTWhileStatement();
    void accept(PTVisitor *);

    PTLogicExpression * expression();
    PTStatements * statements();

    void setExpression(PTLogicExpression *);
    void setStatements(PTStatements*);
private:
    PTLogicExpression * _expression;
    PTStatements * _statements;




};

#endif // PTWHILESTATEMENT_H
