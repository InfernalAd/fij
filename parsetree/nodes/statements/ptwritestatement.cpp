#include "ptwritestatement.h"

PTWriteStatement::PTWriteStatement()
{
}

void PTWriteStatement::accept(PTVisitor * visitor)
{
    visitor->visit(this);
}

PTExpression *PTWriteStatement::expression()
{
    return _expression;
}

void PTWriteStatement::setExpression(PTExpression * expr)
{
    _expression = expr;
}
