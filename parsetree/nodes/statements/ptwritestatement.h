#ifndef PTWRITESTATEMENT_H
#define PTWRITESTATEMENT_H
#include "../ptstatement.h"
#include "../ptexpression.h"

class PTWriteStatement:public PTStatement
{
public:
    PTWriteStatement();
    void accept(PTVisitor *);
    PTExpression * expression();
    void setExpression(PTExpression *);
private:
    PTExpression * _expression;
};

#endif // PTWRITESTATEMENT_H
