#ifndef PTNODE_H
#define PTNODE_H

#include "ptvisitor.h"

namespace VAR
{
    enum VariableType
    {
        UNKNOWN,
        INT,
        FLOAT,
        STRING
    };
}

class PTNode
{
public:
    virtual void accept(PTVisitor *)=0;
};

#endif // PTNODE_H
