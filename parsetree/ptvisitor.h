#ifndef PTVISITOR_H
#define PTVISITOR_H

class PTProgram;
class PTBlock;
class PTStatements;
class PTDeclarationStatement;
class PTWhileStatement;
class PTIfStatement;
class PTAssignStatement;
class PTReadStatement;
class PTWriteStatement;
class PTModStatement;
class PTDivStatement;
class PTSqrtStatement;

class PTAddExpression;
class PTSubExpression;
class PTMulExpression;
class PTDivExpression;
class PTParenExpression;
class PTNumberExpression;
class PTStringExpression;
class PTIdentifierExpression;
class PTFloatNumberExpression;
class PTParenExpression;

class PTOrExpression;
class PTNotExpression;
class PTNotEqualExpression;
class PTMoreExpression;
class PTMoreEqualExpression;
class PTLogicParenExpression;
class PTLessExpression;
class PTLessEqualExpression;
class PTEqualExpression;
class PTAndExpression;

class PTVisitor
{
public:
    virtual void visit(PTProgram *)=0;
    virtual void visit(PTBlock *)=0;
    virtual void visit(PTStatements *)=0;
    virtual void visit(PTDeclarationStatement *)=0;
    virtual void visit(PTWhileStatement *)=0;
    virtual void visit(PTIfStatement *)=0;
    virtual void visit(PTAssignStatement *)=0;
    virtual void visit(PTReadStatement *)=0;
    virtual void visit(PTWriteStatement *)=0;
    virtual void visit(PTModStatement *)=0;
    virtual void visit(PTDivStatement *)=0;
    virtual void visit(PTSqrtStatement *)=0;


    virtual void visit(PTAddExpression *)=0;
    virtual void visit(PTSubExpression *)=0;
    virtual void visit(PTMulExpression *)=0;
    virtual void visit(PTDivExpression *)=0;

    virtual void visit(PTNumberExpression *)=0;
    virtual void visit(PTFloatNumberExpression *)=0;
    virtual void visit(PTStringExpression *)=0;
    virtual void visit(PTIdentifierExpression *)=0;
    virtual void visit(PTParenExpression *)=0;

    virtual void visit(PTOrExpression *)=0;
    virtual void visit(PTNotExpression *)=0;
    virtual void visit(PTNotEqualExpression *)=0;
    virtual void visit(PTMoreExpression *)=0;
    virtual void visit(PTMoreEqualExpression *)=0;
    virtual void visit(PTLogicParenExpression *)=0;
    virtual void visit(PTLessExpression *)=0;
    virtual void visit(PTLessEqualExpression *)=0;
    virtual void visit(PTEqualExpression *)=0;
    virtual void visit(PTAndExpression *)=0;
};

#endif // PTVISITOR_H
